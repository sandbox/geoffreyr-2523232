# WYSIWYG CKEditor Plugins

When using the WYSIWYG module with CKEditor, this module lets you specify 
arbitrary CKEditor plugins that can be recognised by WYSIWYG.

## Rationale

There are quite a few Drupal modules out there exist as shims to allow 
CKEditor plugins to be recognised by WYSIWYG's CKEditor interface. A good 
example of this is 
[WYSIWYG Abbreviation](https://www.drupal.org/project/wysiwyg_abbr), 
which supports one CKEditor plugin (that happens to be built into that 
module), and does it really well.

But the CKEditor system is pretty deep. At the time I wrote this there are 
347 plugins; there's no way you'd want a Drupal module for each and every 
one.

I wrote this module to provide a simple interface to tell WYSIWYG that 
certain CKEditor plugins exist, so it can go ahead and use them. Here's a 
step-by-step guide to how to get a simple plugin up and running.

## Simple Example: Table Resize

Let's say you want to use the 
[Table Resize](http://ckeditor.com/addon/tableresize) plugin to let you 
resize your table columns. After you've installed WYSIWYG, CKEditor, 
and this module, download the plugin to your 
`sites/all/libraries/ckeditor/plugins` directory and unzip it.

Next, go to your WYSIWYG Profiles list.  
Click the *CKEditor Plugins* tab.

You'll be taken to a table showing all the CKEditor Plugins you have 
defined. Right now there are none.  
Click *Add Plugin*.

This will add a new row to the table.  
Fill in each of the cells with the following:

* *Plugin ID:* tableresize
* *Name:* Table Resize
* *Description:* Resize your tables within the editor
* *Path to Plugin:* tableresize
* *Buttons:* _(leave blank)_

This assumes you have a folder called `tableresize` 
containing the file `plugin.js`.  
Click *Save*.

Now you've told Drupal that this CKEditor Plugin exists. 
Time to enable it in your WYSIWYG Profile.  
Click *List* to return to your list of WYSIWYG Profiles.  
Click *Edit* next to the name of the profile you want to add this plugin to.

Once you can see the list of plugins, scroll down to the 
*Table Resize* checkbox.  
Check it, and click *Save*.

That's it, you can start dragging table cells around.

## Custom Buttons: YouTube

Most of the time, for simple plugins, the name of the button will be the 
same as the name of the plugin itself, and will automatically be picked up. 
Sometimes, though, it's not that simple.

The [YouTube plugin](http://ckeditor.com/addon/youtube) is a more complex 
example, simply because it requires you to specify a button by name to add. 
While this module tries to take an educated guess where possible, it's not 
smart enough (yet...) to take a look inside the plugin code and work out 
what buttons to add. However, if the installation instructions ask you to 
specify a button by name, you can do it with this module.

We'll skip ahead to the bit where we need to define the plugin, with an 
empty row in the editor. Fill it in like so:

* *Plugin ID:* youtube
* *Name:* Youtube
* *Description:* Add YouTube videos
* *Path to Plugin:* youtube
* *Buttons:* Youtube

It's very important that the names of each item in the Buttons list matches 
what's specified in the installation instructions for the plugin _exactly_.  
*Save* the plugins, and return to the *WYSIWYG Profile* page 
to add the plugin to.

Note the new button in the list of available plugins.  
Check the checkbox next to the *Youtube* button, and click *Save*.

When you return to your editor, you now have a *Youtube* button. 
Click it to start adding videos to your WYSIWYG text fields.

## Configuration Management

The configuration for this module has been made as simple as possible: 
two Drupal variables.

* wysiwyg_ckeditor_plugins
* wysiwyg_ckeditor_plugin_root

This ensures that you can use 
[Strongarm](https://www.drupal.org/project/strongarm) to package them 
into Features, export them by hand, or manage them however you like 
without having to think too hard about it.