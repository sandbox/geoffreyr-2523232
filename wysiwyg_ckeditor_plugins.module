<?php

/**
 * Adds the selected plugins to WYSIWYG's configuration.
 * Only recognises CKEditor plugins.
 */
function wysiwyg_ckeditor_plugins_wysiwyg_plugin($editor, $version) {
  $plugins = array();
  switch ($editor) {
    // Only do this for ckeditor
    case 'ckeditor':
      $using_plugins = variable_get('wysiwyg_ckeditor_plugins', array());
      $plugin_root = variable_get('wysiwyg_ckeditor_plugin_root');

      // Make each specified plugin available to WYSIWYG
      foreach ($using_plugins as $plname => $plugin) {
        $buttons = array($plname => $plugin['name']);
        if (!empty($plugin['buttons'])) {
          $button_bits = explode(',', $plugin['buttons']);
          foreach ($button_bits as $button) {
            $buttons[$button] = trim($button);
          }
        }

        $plugins[$plname] = array(
          'url'      => '',
          'path'     => $plugin_root . '/' . $plugin['dir'],
          'filename' => 'plugin.js',
          'buttons'  => $buttons,
          'load'     => TRUE,
          'internal' => FALSE,
        );
      }
      break;
  }
  return $plugins;
}

/**
 * Implements hook_menu().
 * Adds a link to the administration page.
 */
function wysiwyg_ckeditor_plugins_menu() {
  $items = array();

  $items['admin/config/content/wysiwyg/plugins-ckeditor'] = array(
    'title' => 'CKEditor Plugins',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wysiwyg_ckeditor_plugins_form'),
    'description' => 'Configure CKEditor plugins for the WYSIWYG editor.',
    'access arguments' => array('administer filters'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Form builder for Wysiwyg CKEditor Plugins form.
 */
function wysiwyg_ckeditor_plugins_form($form, &$form_state) {
  $changes_pending = !empty($form_state['changes_pending']) && $form_state['changes_pending'];

  if (!$changes_pending && empty($form_state['using_plugins'])) {
    $form_state['using_plugins'] = variable_get('wysiwyg_ckeditor_plugins', array());
  }

  if ($changes_pending) {
    drupal_set_message(t("You have add/remove changes pending. Make sure to Save your changes."), "warning");
  }

  // Keep this hidden so we can display the fields in a table
  $form['plugin_info'] = array(
    '#tree'    => TRUE,
    '#weight'  => 999,
    '#printed' => TRUE,
  );

  // This table will be controlled by AJAX
  $req_marker = theme('form_required_marker', array());
  $form['plugins'] = array(
    '#prefix' => '<div id="wysiwyg-ckeditor-current-plugins">',
    '#suffix' => '</div>',
    '#theme'  => 'table',
    '#header' => array(
      array('data' => t("Plugin ID") . $req_marker),
      array('data' => t("Name") . $req_marker),
      array('data' => t("Description") . $req_marker),
      array('data' => t("Path to Plugin") . $req_marker),
      array('data' => t("Buttons (comma-separated)")),
      array('data' => t("Actions"))
    ),
    '#rows'    => array(),
    '#tree'    => TRUE,
  );

  // Transform all available plugins into form rows
  foreach ($form_state['using_plugins'] as $plname => $plugin) {
    $row = _wysiwyg_ckeditor_plugins_form_make_plugin($plname, $plugin);

    $form['plugin_info'][$plname] = array(
      'data' => array()
    );

    foreach ($row as $field_name => $field) {
      $form['plugin_info'][$plname][$field_name] = $field;
      $form['plugins']['#rows'][$plname]['data'][] = array(
        'data' => &$form['plugin_info'][$plname][$field_name]
      );
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['add_plugin'] = array(
    '#type'  => 'submit',
    '#value' => t('Add Plugin'),
    '#submit' => array('_wysiwyg_ckeditor_plugins_form_add'),
    '#ajax'  => array(
      'callback' => '_wysiwyg_ckeditor_plugins_form_list',
      'wrapper'  => 'wysiwyg-ckeditor-current-plugins',
      'method'   => 'replace',
      'effect'   => 'fade',
    ),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/content/wysiwyg',
  );

  return $form;
}
/**
 * AJAX Callback: List all plugins.
 */
function _wysiwyg_ckeditor_plugins_form_list($form, $form_state) {
  return $form['plugins'];
}

/**
 * Form submit handler for AJAX Callback:
 * Add a new row for a plugin.
 */
function _wysiwyg_ckeditor_plugins_form_add($form, &$form_state) {
  $nr = count($form_state['using_plugins']);
  $form_state['using_plugins'][$nr] = array();
  $form_state['changes_pending'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Form submit handler for AJAX Callback:
 * Remove a plugin.
 */
function _wysiwyg_ckeditor_plugins_form_delete($form, &$form_state) {
  $delete_key = preg_replace('/^delete__/', '', $form_state['input']['_triggering_element_name']);
  if (isset($form_state['using_plugins'][$delete_key])) {
    unset($form_state['using_plugins'][$delete_key]);
  }
  $form_state['changes_pending'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Generate form fields for a plugin.
 */
function _wysiwyg_ckeditor_plugins_form_make_plugin($plname, $plugin = array()) {
  $plugin_root = variable_get('wysiwyg_ckeditor_plugin_root');
  $p = array();
  $p['id'] = array(
    '#type'          => is_numeric($plname) ? 'textfield' : 'value',
    '#title'         => t("Plugin ID"),
    '#title_display' => 'invisible',
    '#value'         => is_numeric($plname) ? "" : $plname,
    '#size'          => 32,
    '#maxlength'     => 255,
    '#required'      => TRUE,
    '#prefix'        => is_numeric($plname) ? "" : $plname,
  );
  $p['name'] = array(
    '#type'          => 'textfield',
    '#title'         => t("Name of Plugin"),
    '#title_display' => 'invisible',
    '#value'         => !empty($plugin['name']) ? $plugin['name'] : "",
    '#size'          => 32,
    '#required'      => TRUE,
  );
  $p['desc'] = array(
    '#type'          => 'textfield',
    '#title'         => t("Description"),
    '#title_display' => 'invisible',
    '#value'         => !empty($plugin['desc']) ? $plugin['desc'] : "",
    '#size'          => 32,
    '#required'      => TRUE,
  );
  $p['dir'] = array(
    '#type'          => 'textfield',
    '#title'         => t("Base Directory"),
    '#title_display' => 'invisible',
    '#value'         => !empty($plugin['dir']) ? $plugin['dir'] : "",
    '#size'          => 32,
    '#required'      => TRUE,
  );
  $p['buttons'] = array(
    '#type'          => 'textfield',
    '#title'         => t("Buttons (comma-separated)"),
    '#title_display' => 'invisible',
    '#value'         => !empty($plugin['buttons']) ? $plugin['buttons'] : "",
    '#size'          => 32,
    '#required'      => FALSE,
    '#attributes'    => array(
      'placeholder'    => is_numeric($plname) ? "Name of buttons (if you need to set them)" : $plname,
    ),
  );
  $p['delete'] = array(
    '#type'   => 'submit',
    '#name'   => 'delete__' . $plname,
    '#value'  => t('Delete'),
    '#submit' => array('_wysiwyg_ckeditor_plugins_form_delete'),
    '#ajax'   => array(
      'callback' => '_wysiwyg_ckeditor_plugins_form_list',
      'wrapper'  => 'wysiwyg-ckeditor-current-plugins',
      'method'   => 'replace',
      'effect'   => 'fade',
    ),
  );
  return $p;
}

/**
 * Form submit handler for AJAX Callback:
 * Save the list of plugins.
 */
function wysiwyg_ckeditor_plugins_form_submit($form, &$form_state) {
  if (!empty($form_state['input']['plugin_info'])) {
    
    $using_plugins = array();

    // Generate list of plugin data to be saved
    foreach ($form_state['input']['plugin_info'] as $k => $row) {
      if (!empty($row['id'])) {
        $key = $row['id'];
      }
      else {
        $key = $k;
      }
      $using_plugins[$key] = $row;
    }

    unset($form_state['changes_pending']);

    variable_set('wysiwyg_ckeditor_plugins', $using_plugins);
    drupal_set_message(t('Saved plugin settings.'), 'status');
  }
  else {
    variable_set('wysiwyg_ckeditor_plugins', array());
    drupal_set_message(t('Cleared plugin settings.'), 'status'); 
  }
}